""" Script to link AgriBalyse data to OFF products """

import copy
import os
import json

import tqdm
from fuzzywuzzy import process
from fuzzywuzzy import fuzz
import pandas as pd

from vars import AGRIBALYSE_DATADIR, TAXONOMY_FILEPATH, AGRIBALYSE_PREBINDING_FILEPATH,\
    AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH

MINIMUM_SCORE = 65  # Minimum fuzzy matching score for a match to be considered

process_names = pd.read_table(os.path.join(AGRIBALYSE_DATADIR, 'all_processes.txt'), header=None)[0].to_list()

with open(TAXONOMY_FILEPATH, 'r') as file:
    taxonomy = json.load(file)

data_bound_from_ciqual = pd.read_csv(AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH)
off_duplicates = pd.read_csv(os.path.join(AGRIBALYSE_DATADIR, 'off-duplicates.csv'))

# Words to be removed from the process names before matching
STOP_WORDS = ['processed in FR', 'PVC', 'Steel', 'PS', 'LDPE', 'at farm gate', 'FR', 'Cardboard', 'processed in ',
              'at processing', 'Ambient (long)', 'Ambient (average)', 'Ambient (short)', 'at packaging',
              'consumption mix', 'Chilled', 'Conventional', 'rain cover', 'first production years (phase)',
              'at orchard', 'at plant', 'canned,', 'canned/', 'drained,', 'national average', 'conventional',
              'system n°', 'semi-preserved,', 'electric platform', 'fixed spraying system', 'scab-tolerant',
              'full production years (phase)', 'plantation and destruction (phase)',
              '|', '/']

# Removing processes other than "at packaging" if the "at packaging" process exists, and removing stopwords
# Poorly optimized
for process_name in tqdm.tqdm(copy.deepcopy(process_names)):
    if '| at packaging/FR' in process_name:
        name_root = process_name.replace('| at packaging/FR', '')
        for duplicate in [x for x in process_names if (name_root in x) and ('| at packaging/FR' not in x)]:
            process_names.remove(duplicate)

# Removing stopwords
# Storing the simplified version and the raw version of process names in a dict, raw version can then be retrieved by
# getting the dict value
simplified_names = dict()
for process_name in tqdm.tqdm(process_names):
    simplified_name = process_name
    for word in STOP_WORDS:
        simplified_name = simplified_name.replace(word, '')

    simplified_names[simplified_name] = process_name

# with open(os.path.join(AGRIBALYSE_DATADIR, 'processed_names.csv'), 'w') as file:
#     for k, v in simplified_names.items():
#         file.write(k + ';' + v + '\n')

# Looping on off ingredients trying to match with a process name
result = []
unbound_data = []
for off_id, ingredient in tqdm.tqdm(taxonomy.items()):
    if any(off_id == data_bound_from_ciqual.OFF_ID) or any(off_id == off_duplicates.ingredient):
        continue

    if 'en' in ingredient['name']:
        matches = process.extract(ingredient['name']['en'], simplified_names.keys(),
                                  limit=50, scorer=fuzz.token_sort_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            result.append({'OFF_EN': ingredient['name']['en'],
                           'AGRIBALYSE_EN': simplified_names[match[0]],
                           'OFF_FR': ingredient['name'].get('fr'),
                           'OFF_ID': off_id})

        if len(matches) == 0:
            unbound_data.append({'OFF_FR': ingredient['name'].get('fr', ''),
                                 'OFF_EN': ingredient['name']['en'],
                                 'OFF_ID': off_id})

    else:
        unbound_data.append({'OFF_FR': ingredient['name'].get('fr', ''),
                             'OFF_EN': '',
                             'OFF_ID': off_id})

pd.DataFrame(result).to_csv(AGRIBALYSE_PREBINDING_FILEPATH, index=False)
unbound_data.sort(key=lambda x: x['OFF_ID'])
pd.DataFrame(unbound_data).to_csv(os.path.join(AGRIBALYSE_DATADIR, 'unbound_data.csv'), index=False)

print(len(unbound_data))
