""" Linking off data with AgriBalyse LCIs """

import copy
import os
import json

import pandas as pd

from vars import AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH, AGRIBALYSE_PREBINDING_FILEPATH, \
    INGREDIENTS_DATA_FILEPATH, DATADIR

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

agb_binding_from_ciqual = pd.read_csv(AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH)
agb_binding_fuzzy = pd.read_csv(AGRIBALYSE_PREBINDING_FILEPATH)  # Binding by fuzzy string matching

used_processes = set()

for binding in agb_binding_from_ciqual.itertuples():
    if binding.OFF_ID not in ingredients_data:
        ingredients_data[binding.OFF_ID] = dict()

    if 'LCI' not in ingredients_data[binding.OFF_ID]:
        ingredients_data[binding.OFF_ID]['LCI'] = []

    ingredients_data[binding.OFF_ID]['LCI'].append(binding.PROCESS_NAME)
    used_processes.add(binding.PROCESS_NAME)

for binding in agb_binding_fuzzy.itertuples():
    if binding.OFF_ID not in ingredients_data:
        ingredients_data[binding.OFF_ID] = dict()

    if 'LCI' not in ingredients_data[binding.OFF_ID]:
        ingredients_data[binding.OFF_ID]['LCI'] = []

    ingredients_data[binding.OFF_ID]['LCI'].append(binding.AGRIBALYSE_EN)
    used_processes.add(binding.AGRIBALYSE_EN)

# Looping on all duplicates to process and add them to the result
duplicates = pd.read_csv(os.path.join(DATADIR, 'off_agribalyse_binding', 'off-duplicates.csv'))
for duplicate in duplicates.itertuples():
    proxy = copy.deepcopy(ingredients_data[duplicate.reference])
    proxy['environmental_impact_proxy'] = duplicate.reference
    ingredients_data[duplicate.ingredient] = proxy

with open(INGREDIENTS_DATA_FILEPATH, 'w') as file:
    json.dump(ingredients_data, file, indent=2)
