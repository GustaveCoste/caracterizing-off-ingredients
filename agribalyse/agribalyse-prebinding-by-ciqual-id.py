""" Script to link AgriBalyse data to OFF products using the ciqual binding"""

import os

import tqdm
import pandas as pd

from vars import AGRIBALYSE_DATADIR, OFF_CIQUAL_PREBINDING_FILEPATH, AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH

process_names = pd.read_table(os.path.join(AGRIBALYSE_DATADIR, 'all_processes.txt'), header=None)[0]
ciqual_bindings = pd.read_csv(OFF_CIQUAL_PREBINDING_FILEPATH)

result = []
for binding in tqdm.tqdm(ciqual_bindings.itertuples()):
    try:
        process_name = process_names[process_names.str.contains(f"[Ciqual code: {binding.CIQUAL_ID}]", regex=False)] \
            .iloc[0]
    except IndexError:  # Not match found
        continue

    at_packaging_process = process_name.replace(f"at consumer/FR [Ciqual code: {binding.CIQUAL_ID}]",
                                                "at packaging/FR") \
        .replace('No preparation | ', '') \
        .replace('Boiling | ', '') \
        .replace('Pan frying | ', '') \
        .replace('Chilled at consumer | ', '') \
        .replace('Water cooker | ', '') \
        .replace('Microwave | ', '') \
        .replace('Oven | ', '')
    if any(process_names == at_packaging_process):
        process_name = at_packaging_process
    else:
        print(process_name)

    result.append({'OFF_FR': binding.OFF_FR,
                   'CIQUAL_FR': binding.CIQUAL_FR,
                   'OFF_EN': binding.OFF_EN,
                   'CIQUAL_EN': binding.CIQUAL_EN,
                   'OFF_ID': binding.OFF_ID,
                   'PROCESS_NAME': process_name})

pd.DataFrame(result).to_csv(os.path.join(AGRIBALYSE_PREBINDING_FROM_CIQUAL_FILEPATH))
