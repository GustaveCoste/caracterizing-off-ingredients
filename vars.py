# Nutriment categories taken into account for the composition resolution
TOP_LEVEL_NUTRIMENTS_CATEGORIES = ['proteins',
                                   'carbohydrates',
                                   'fat',
                                   'fiber',
                                   'salt',
                                   'other']

NUTRIMENTS_CATEGORIES = TOP_LEVEL_NUTRIMENTS_CATEGORIES + ['sugars',
                                                           'saturated-fat']

# Max ash content of ingredients in %
MAX_ASH_CONTENT = 10

NUTRIMENTS_ENERGY_CONTENT = {  # From EU directives, in kcal/g
    'proteins': 4,
    'carbohydrates': 4,
    'fat': 9,
    'fiber': 2
}

# Ciqual
# Conversion from ciqual nutritional data identifiers to off
CIQUAL_TO_OFF = {'400': 'water',
                 '328': 'energy-kcal',
                 '25000': 'proteins',
                 '31000': 'carbohydrates',
                 '40000': 'fat',
                 '32000': 'sugars',
                 '34100': 'fiber',
                 '40302': 'saturated-fat',
                 '10004': 'salt'}

# Error margin related to nutritional confidence codes (not used with CIQUAL data)
NUTRI_CONFIDENCE_MARGINS = {'A': 0,
                            'B': 0.1,
                            'C': 0.25,
                            'D': 0.5}


# Conversion from FCEN nutritional data identifiers to off
FCEN_TO_OFF = {255: 'water',
               208: 'energy-kcal',
               203: 'proteins',
               205: 'carbohydrates',
               204: 'fat',
               269: 'sugars',
               291: 'fiber',
               606: 'saturated-fat',
               207: 'ash'}
