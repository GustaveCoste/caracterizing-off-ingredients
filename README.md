This repository is outdated, see [framagit.org/GustaveCoste/off-product-environmental-impact](https://framagit.org/GustaveCoste/off-product-environmental-impact).


--------------------------

This repository contains scripts and results created by Gustave Coste at INRAE in order to link ingredients from the Open Food Facts database to the nutritional databases Ciqual and FCEN and the life cycle inventory database Agribalyse. It is provided as is with no warranty and should not be considered as reference data.

Contact: gustave.coste@inrae.fr
