""" Worst piece of code ever... """

import os
import json
import csv

from vars import TAXONOMY_FILEPATH, INGREDIENTS_DATA_FILEPATH, INGREDIENTS_IMPORTANCE_FILEPATH, DATADIR

usage = dict()
with open(INGREDIENTS_IMPORTANCE_FILEPATH, 'r') as file:
    for row in csv.reader(file):
        if row[0] == 'id':
            continue
        usage[row[0]] = int(row[1])

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

with open(TAXONOMY_FILEPATH, 'r') as file:
    taxonomy = json.load(file)

a = {k: v for k, v in taxonomy.items() if k not in ingredients_data}
b = [(v.get('name', {}).get('fr', ''),
      v.get('name', {}).get('en', ''),
      '|'.join(v.get('parents', [])),
      k,
      usage.get(k, 0))
     for k, v in a.items()]

b.sort(key=lambda x: x[4], reverse=True)

with open(os.path.join(DATADIR, 'nutri_from_manual_editing', 'unbound_data.csv'), 'w') as file:
    csv.writer(file).writerow(['FR', 'EN', 'PARENTS', 'OFF_ID', 'USAGE'])
    csv.writer(file).writerows(b)

