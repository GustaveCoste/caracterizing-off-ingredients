""" Script to pre-identify matches between off and ciqual ingredients """

import os
import json

import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import tqdm

from vars import CIQUAL_DATA_DIR, TAXONOMY_FILEPATH, OFF_CIQUAL_PREBINDING_FILEPATH

MINIMUM_SCORE = 80  # Minimum fuzzy matching score for a match to be considered

with open(os.path.join(CIQUAL_DATA_DIR, 'ciqual_data.json'), 'r') as file:
    ciqual_data = json.load(file)

with open(TAXONOMY_FILEPATH, 'r') as file:
    taxonomy = json.load(file)

ciqual_names = pd.DataFrame([{'id': x['alim_code'],
                              'fr': x['alim_nom_fr'],
                              'index_fr': x['alim_nom_index_fr'],
                              'en': x['alim_nom_eng']}
                             for x in ciqual_data.values()])

result = pd.DataFrame(columns=['OFF_FR', 'CIQUAL_FR', 'CIQUAL_INDEX_FR', 'OFF_EN', 'CIQUAL_EN', 'OFF_ID', 'CIQUAL_ID'])
for off_id, ingredient in tqdm.tqdm(taxonomy.items()):

    if 'fr' in ingredient['name']:
        # Matching on french name
        matches = process.extract(ingredient['name']['fr'], ciqual_names['fr'],
                                  limit=20, scorer=fuzz.token_set_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            result = result.append(pd.DataFrame({'OFF_FR': ingredient['name']['fr'],
                                                 'CIQUAL_FR': match[0],
                                                 'CIQUAL_INDEX_FR': ciqual_names[ciqual_names['fr'] == match[0]][
                                                     'index_fr'],
                                                 'OFF_EN': ingredient['name'].get('en'),
                                                 'CIQUAL_EN': ciqual_names[ciqual_names['fr'] == match[0]]['en'],
                                                 'OFF_ID': off_id,
                                                 'CIQUAL_ID': ciqual_names[ciqual_names['fr'] == match[0]]['id']}))

        # Matching on Ciqual index name (can be different)
        matches = process.extract(ingredient['name']['fr'], ciqual_names['index_fr'],
                                  limit=20, scorer=fuzz.token_set_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            for alim in ciqual_names[ciqual_names['index_fr'] == match[0]]['fr'].values:
                result = result.append(pd.DataFrame({'OFF_FR': ingredient['name']['fr'],
                                                     'CIQUAL_FR': alim,
                                                     'CIQUAL_INDEX_FR': match[0],
                                                     'OFF_EN': ingredient['name'].get('en'),
                                                     'CIQUAL_EN': ciqual_names[ciqual_names['fr'] == alim]['en'],
                                                     'OFF_ID': off_id,
                                                     'CIQUAL_ID': ciqual_names[ciqual_names['fr'] == alim]['id']}))

    if 'en' in ingredient['name']:
        # Matching on english name
        matches = process.extract(ingredient['name']['en'], ciqual_names['en'],
                                  limit=20, scorer=fuzz.token_set_ratio)
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            result = result.append(pd.DataFrame({'OFF_FR': ingredient['name'].get('fr'),
                                                 'CIQUAL_FR': ciqual_names[ciqual_names['en'] == match[0]]['fr'],
                                                 'CIQUAL_INDEX_FR': ciqual_names[ciqual_names['en'] == match[0]][
                                                     'index_fr'],
                                                 'OFF_EN': ingredient['name']['en'],
                                                 'CIQUAL_EN': match[0],
                                                 'OFF_ID': off_id,
                                                 'CIQUAL_ID': ciqual_names[ciqual_names['en'] == match[0]]['id']}))


# Removing duplicates and sorting by OFF id
result.drop_duplicates(inplace=True)
result.sort_values(by='OFF_ID', inplace=True)

result.to_csv(OFF_CIQUAL_PREBINDING_FILEPATH, index=False)
