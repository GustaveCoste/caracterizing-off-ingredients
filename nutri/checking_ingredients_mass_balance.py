""" Script to check for all ingredients that their nutritional data are consistent. """

import json

from vars import INGREDIENTS_DATA_FILEPATH, TOP_LEVEL_NUTRIMENTS_CATEGORIES

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

nutri_categories = TOP_LEVEL_NUTRIMENTS_CATEGORIES + ['water']

too_high_min = []
too_low_max = []
for ingredient in ingredients_data.values():
    if 'nutriments' not in ingredient:
        continue

    if all([x in ingredient['nutriments'] for x in nutri_categories]):

        # If the sum of the reference value for all nutriments is not 100, rectify the data
        ref_value_sum = sum([ingredient['nutriments'][nutri]['value'] for nutri in nutri_categories])
        if ref_value_sum != 100:
            for nutri in nutri_categories:
                if ingredient['nutriments'][nutri]['min'] == ingredient['nutriments'][nutri]['max'] == \
                        ingredient['nutriments'][nutri]['value']:
                    ingredient['nutriments'][nutri]['min'] = \
                        ingredient['nutriments'][nutri]['max'] = \
                        ingredient['nutriments'][nutri]['value'] = \
                        ingredient['nutriments'][nutri]['value'] * 100 / ref_value_sum
                else:
                    ingredient['nutriments'][nutri]['value'] = \
                        ingredient['nutriments'][nutri]['value'] * 100 / ref_value_sum

        # If the sum of minimum values for all nutriments is higher than 100, there is a problem
        min_sum = sum([ingredient['nutriments'][nutri]['min'] for nutri in nutri_categories])
        if min_sum > 100.00000001:
            too_high_min.append((min_sum, ingredient))

        # If the sum of maximum values for all nutriments is lower than 100, there is a problem
        max_sum = sum([ingredient['nutriments'][nutri]['max'] for nutri in nutri_categories])
        if max_sum < 99.9999:
            too_low_max.append((max_sum, ingredient))

print(too_high_min)
print(too_low_max)
