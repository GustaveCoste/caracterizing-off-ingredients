import os
import pandas as pd
import json

from vars import FCEN_DATA_DIR, FCEN_TO_OFF

food_names = pd.read_csv(os.path.join(FCEN_DATA_DIR, 'FOOD NAME.csv'), encoding='ISO-8859-1')
nutrient_amounts = pd.read_csv(os.path.join(FCEN_DATA_DIR, 'NUTRIENT AMOUNT.csv'), encoding='ISO-8859-1')

top_level_nutriments = ['proteins', 'carbohydrates', 'fat', 'fiber', 'water']

result = dict()
for i, food in food_names.iterrows():
    nutriments = dict()

    for j, nutri_amount in nutrient_amounts[(nutrient_amounts.FoodID == food.FoodID) &
                                            (nutrient_amounts.NutrientID.isin(FCEN_TO_OFF.keys()))].iterrows():
        nutriments[FCEN_TO_OFF[nutri_amount.NutrientID]] = {"value": nutri_amount.NutrientValue,
                                                            "stdev": nutri_amount.StandardError}

    nutriments_sum = sum([v['value'] for k, v in nutriments.items() if k in top_level_nutriments])
    nutriments['other'] = {'value': 0, 'stdev': 0}

    # If the total sum is superior to 100, rectify the values
    if nutriments_sum > 100:
        for nutri in nutriments.values():
            if nutri['value']:
                nutri['value'] = nutri['value'] * 100 / nutriments_sum

    # If the total sum is inferior to 100, add a "other" nutriment category that will ensure mass balance
    elif (nutriments_sum < 100) and (all([x in nutriments for x in top_level_nutriments])):
        nutriments['other']['value'] = 100 - nutriments_sum

    food['nutriments'] = nutriments

    result[food.FoodID] = food.to_dict()

with open(os.path.join(FCEN_DATA_DIR, 'fcen_data.json'), 'w') as file:
    json.dump(result, file, indent=2, ensure_ascii=True)
