""" Script to pre-identify matches between off and the FCEN (Fichier Canadien des Élements Nutritifs) ingredients """

import os
import json

import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import tqdm

from vars import FCEN_DATA_DIR, TAXONOMY_FILEPATH, OFF_FCEN_PREBINDING_FILEPATH, INGREDIENTS_DATA_FILEPATH

MINIMUM_SCORE = 80  # Minimum fuzzy matching score for a match to be considered

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

with open(TAXONOMY_FILEPATH, 'r') as file:
    taxonomy = json.load(file)

fcen_data = pd.read_json(os.path.join(FCEN_DATA_DIR, 'fcen_data.json')).transpose()

result = pd.DataFrame(columns=['OFF_FR', 'FCEN_FR', 'OFF_EN', 'FCEN_EN', 'OFF_ID', 'FCEN_ID'])
for off_id, ingredient in tqdm.tqdm(taxonomy.items()):

    # If the ingredient already has data (from CIQUAL), skip it
    if off_id in ingredients_data:
        if 'nutriments' in ingredients_data[off_id]:
            continue

    if 'fr' in ingredient['name']:
        # Matching on french name
        matches = process.extract(ingredient['name']['fr'], fcen_data['FoodDescriptionF'],
                                  limit=20, scorer=fuzz.token_set_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            result = result.append(pd.DataFrame(
                {'OFF_FR': ingredient['name']['fr'],
                 'FCEN_FR': match[0],
                 'OFF_EN': ingredient['name'].get('en'),
                 'FCEN_EN': fcen_data[fcen_data['FoodDescriptionF'] == match[0]]['FoodDescription'],
                 'OFF_ID': off_id,
                 'FCEN_ID': fcen_data[fcen_data['FoodDescriptionF'] == match[0]]['FoodID']}))

    if 'en' in ingredient['name']:
        # Matching on english name
        matches = process.extract(ingredient['name']['en'], fcen_data['FoodDescription'],
                                  limit=20, scorer=fuzz.token_set_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE]

        for match in matches:
            result = result.append(pd.DataFrame(
                {'OFF_FR': ingredient['name'].get('fr'),
                 'FCEN_FR': fcen_data[fcen_data['FoodDescription'] == match[0]]['FoodDescriptionF'],
                 'OFF_EN': ingredient['name'].get('en'),
                 'FCEN_EN': match[0],
                 'OFF_ID': off_id,
                 'FCEN_ID': fcen_data[fcen_data['FoodDescription'] == match[0]]['FoodID']}))

result.drop_duplicates(inplace=True)
result.sort_values(by='OFF_ID', inplace=True)

result.to_csv(OFF_FCEN_PREBINDING_FILEPATH, index=False)
