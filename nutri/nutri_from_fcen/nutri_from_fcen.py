""" Script to link FCEN nutritional data to OFF ingredients """

import json
import os
import random
from statistics import mean
import math
import copy

import pandas as pd

from vars import FCEN_DATA_DIR, DATADIR, NUTRIMENTS_ENERGY_CONTENT

bindings = pd.read_csv(os.path.join(DATADIR, 'off_fcen_binding', 'off_fcen_binding.csv'))

with open(os.path.join(FCEN_DATA_DIR, 'fcen_data.json'), 'r') as file:
    fcen_data = json.load(file)

with open(os.path.join(DATADIR, 'ingredients_data.json'), 'r') as file:
    ingredients_data = json.load(file)

# Looping on all bindings to append fcen data to off ingredients
for off_id in bindings.OFF_ID.unique():

    ingredient = {'source': 'fcen'}
    nutriments = dict()

    # Looping on fcen products related to this off ingredient
    fcen_ids = list(bindings[bindings.OFF_ID == off_id].FCEN_ID)
    if len(fcen_ids) == 0:
        continue
    elif len(fcen_ids) == 1:
        fcen_nutriments = fcen_data[str(fcen_ids[0])]['nutriments']

        # Getting minimum and maximum value for each nutriment using the standard deviation
        # The distribution of the nutriment amount is supposed to be normal and the minimum and maximum values are
        # defined as:
        # min/max = reference_value -/+ 2 * std_error
        # If no standard error is given, a default 10% margin is used

        # Getting minimum and maximum value for each nutriment
        # If they are not present, uses the reference value
        for nutriment_name in fcen_nutriments:
            value = fcen_nutriments[nutriment_name]['value']
            stdev = fcen_nutriments[nutriment_name]['stdev']

            if not math.isnan(stdev):
                min_value = value - (2 * stdev)
                max_value = value + (2 * stdev)
            else:
                min_value = value
                max_value = value

            max_value = min(max_value, 100) if nutriment_name != 'energy-kcal' else max_value

            nutriments[nutriment_name] = {'value': value, 'min': min_value, 'max': max_value}

    else:
        # If there are more than one fcen product linked to this off ingredient,
        # compile the data of every fcen product
        values = dict()
        min_values = dict()
        max_values = dict()
        for fcen_id in fcen_ids:
            fcen_product = fcen_data[str(fcen_id)]

            for nutriment_name, nutriment_data in fcen_product['nutriments'].items():
                if nutriment_name not in values:
                    values[nutriment_name] = []
                    min_values[nutriment_name] = []
                    max_values[nutriment_name] = []

                value = nutriment_data['value']
                stdev = nutriment_data['stdev']

                if not math.isnan(stdev):
                    min_value = value - (2 * stdev)
                    max_value = value + (2 * stdev)
                else:
                    min_value = value
                    max_value = value

                values[nutriment_name].append(value)
                min_values[nutriment_name].append(min_value)
                max_values[nutriment_name].append(min(max_value, 100) if nutriment_name != 'energy-kcal' else max_value)

        for nutriment_name in values.keys():
            if values[nutriment_name]:
                nutriments[nutriment_name] = {'value': mean(values[nutriment_name]),
                                              'min': min(min_values[nutriment_name]),
                                              'max': max(max_values[nutriment_name])}

    if len(nutriments) > 0:
        # If energy is missing, calculate it from the other nutriments
        if ('energy-kcal' not in nutriments) \
                and ('proteins' in nutriments) \
                and ('carbohydrates' in nutriments) \
                and ('fat' in nutriments) \
                and ('fiber' in nutriments):
            nutriments['energy-kcal'] = {'min': sum([nutriments[x]['min'] * NUTRIMENTS_ENERGY_CONTENT[x]
                                                     for x in NUTRIMENTS_ENERGY_CONTENT]),
                                         'max': sum([nutriments[x]['max'] * NUTRIMENTS_ENERGY_CONTENT[x]
                                                     for x in NUTRIMENTS_ENERGY_CONTENT])}

            ingredient['comment'] = ingredient.get('comment', '') + 'Energy calculated from other nutriments'

        # Add the nutriments dict to the ingredient
        ingredient['nutriments'] = nutriments

        # Adding the ingredient to the main result
        ingredients_data[off_id] = ingredient

# Looping on all duplicates to process and add them to the result
duplicates = pd.read_csv(os.path.join(DATADIR, 'off_fcen_binding', 'off-duplicates.csv'))
for duplicate in duplicates.itertuples():
    proxy = copy.deepcopy(ingredients_data[duplicate.reference])
    proxy['nutritional_proxy'] = duplicate.reference
    ingredients_data[duplicate.ingredient] = proxy

with open(os.path.join(DATADIR, 'ingredients_data.json'), 'w') as file:
    json.dump(ingredients_data, file, indent=2)
