import json
import os
import random
from statistics import mean
import math
import copy

import pandas as pd

from vars import CIQUAL_DATA_DIR, FCEN_DATA_DIR, DATADIR, NUTRIMENTS_CATEGORIES, NUTRI_CONFIDENCE_MARGINS

bindings = pd.read_csv(os.path.join(DATADIR, 'nutri_from_manual_editing', 'unbound_data.csv'), na_filter=False)

with open(os.path.join(DATADIR, 'ingredients_data.json'), 'r') as file:
    ingredients_data = json.load(file)

with open(os.path.join(FCEN_DATA_DIR, 'fcen_data.json'), 'r') as file:
    fcen_data = json.load(file)

with open(os.path.join(CIQUAL_DATA_DIR, 'ciqual_data.json'), 'r') as file:
    ciqual_data = json.load(file)

top_level_nutriments = ['proteins', 'carbohydrates', 'fat', 'fiber', 'water']

# Adding data with nutritional values
for binding in bindings[(bindings.CIQUAL_ID == '') & (bindings.FCEN_ID == '') & (bindings.PROXY == '')].itertuples():
    ingredient = dict()
    for nutriment_name in NUTRIMENTS_CATEGORIES + ['energy-kcal']:
        if nutriment_name == 'other':
            continue

        value = getattr(binding, nutriment_name.replace('-', '_'))

        if value != '':
            value = float(value)
            # Computing min and max value
            if binding.CONFIDENCE_CODE != '':
                min_value = value * (1 - NUTRI_CONFIDENCE_MARGINS[binding.CONFIDENCE_CODE])
                max_value = value * (1 + NUTRI_CONFIDENCE_MARGINS[binding.CONFIDENCE_CODE])
                max_value = min(100, max_value)
            else:
                min_value = max_value = value

            # Creating the nutriments dict if it does not exist
            if 'nutriments' not in ingredient:
                ingredient['nutriments'] = dict()

            ingredient['nutriments'][nutriment_name] = {'value': value,
                                                        'min': min_value,
                                                        'max': max_value}
    if len(ingredient) > 0:
        nutriments_sum = sum([v['value'] for k, v in ingredient['nutriments'].items() if k in top_level_nutriments])
        ingredient['nutriments']['other'] = {'value': 0}

        # If the total sum is superior to 100, rectify the values
        if nutriments_sum > 100:
            for nutri in ingredient['nutriments'].values():
                if nutri['value']:
                    nutri['value'] = nutri['value'] * 100 / nutriments_sum

        # If the total sum is inferior to 100, add a "other" nutriment category that will ensure mass balance
        elif (nutriments_sum < 100) and (all([x in ingredient['nutriments'] for x in top_level_nutriments])):
            ingredient['nutriments']['other']['value'] = 100 - nutriments_sum

    if binding.SMALL_QUANTITY != '':
        ingredient['used_in_small_quantity'] = binding.SMALL_QUANTITY

    if binding.SOURCE != '':
        ingredient['source'] = binding.SOURCE

    ingredients_data[binding.OFF_ID] = ingredient

# Binding data with existing proxies
for binding in bindings[bindings.PROXY != ''].itertuples():
    proxy = copy.deepcopy(ingredients_data[binding.PROXY])
    proxy['nutritional_proxy'] = binding.PROXY
    ingredients_data[binding.OFF_ID] = proxy

with open(os.path.join(DATADIR, 'ingredients_data.json'), 'w') as file:
    json.dump(ingredients_data, file, indent=2)
