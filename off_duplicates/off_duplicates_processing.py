import os
import json
import copy

import pandas as pd

from vars import OFF_DUPLICATES_FILEPATH, INGREDIENTS_DATA_FILEPATH, DATADIR, AGRIBALYSE_DATADIR

duplicates = pd.read_csv(OFF_DUPLICATES_FILEPATH)
duplicates.columns = ['ingredient', 'reference', 'proxy_type']

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

for ingredient_name, ingredient_data in ingredients_data.items():
    ingredient_data['id'] = ingredient_name

for duplicate in duplicates.itertuples():
    proxy = copy.deepcopy(ingredients_data[duplicate.reference])
    ingredient = {'id': duplicate.ingredient}

    # Nutritional proxy
    if (duplicate.proxy_type != 2) and ('nutriments' in proxy):
        ingredient['nutriments'] = proxy['nutriments']
        ingredient['nutritional_proxy'] = duplicate.reference

    # Impact proxy
    if (duplicate.proxy_type != 1) and ('LCI' in proxy):
        ingredient['LCI'] = proxy['LCI']
        ingredient['environmental_impact_proxy'] = duplicate.reference

    ingredients_data[duplicate.ingredient] = ingredient


# Rolling up proxies to "root" for all ingredients
def get_root_proxy(ingredient_param, proxy_type_param):
    if proxy_type_param in ingredient_param:
        if ingredient_param[proxy_type_param] == ingredient_param['id']:
            del ingredient_param[proxy_type_param]
            return ingredient_param['id']

        return get_root_proxy(ingredients_data[ingredient_param[proxy_type_param]], proxy_type_param)
    else:
        return ingredient_param['id']


for ingredient_name, ingredient_data in ingredients_data.items():
    if 'nutritional_proxy' in ingredient_data:
        ingredient_data['nutritional_proxy'] = get_root_proxy(ingredient_data, 'nutritional_proxy')
    if 'environmental_impact_proxy' in ingredient_data:
        ingredient_data['environmental_impact_proxy'] = get_root_proxy(ingredient_data, 'environmental_impact_proxy')

with open(INGREDIENTS_DATA_FILEPATH, 'w') as file:
    json.dump(ingredients_data, file, indent=2)

# Getting the list of proxies
with open(os.path.join(DATADIR, 'ingredients-proxies.csv'), 'w') as file:
    file.write(f"ingredient,proxy,proxy_type\n")
    for ingredient in ingredients_data.values():
        if ('nutritional_proxy' in ingredient) and ('environmental_impact_proxy' in ingredient):
            if ingredient['nutritional_proxy'] == ingredient['environmental_impact_proxy']:
                file.write(f"{ingredient['id']},{ingredient['nutritional_proxy']},both\n")
            else:
                file.write(f"{ingredient['id']},{ingredient['nutritional_proxy']},nutrition\n")
                file.write(f"{ingredient['id']},{ingredient['environmental_impact_proxy']},impact\n")
        elif 'nutritional_proxy' in ingredient:
            file.write(f"{ingredient['id']},{ingredient['nutritional_proxy']},nutrition\n")
        elif 'environmental_impact_proxy' in ingredient:
            file.write(f"{ingredient['id']},{ingredient['environmental_impact_proxy']},impact\n")

# Getting the list of AgriBalyse LCIs used
used_processes = set()
for ingredient in ingredients_data.values():
    for lci in ingredient.get('LCI', []):
        used_processes.add(lci)

with open(os.path.join(AGRIBALYSE_DATADIR, 'used_processes.txt'), 'w') as file:
    file.write('\n'.join(used_processes))
