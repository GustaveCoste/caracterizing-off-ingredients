""" Script to identify possible duplicates in the OFF ingredients taxonomy """

import os
import json
import re

from fuzzywuzzy import process
from fuzzywuzzy import fuzz
import pandas as pd
import tqdm

from vars import TAXONOMY_FILEPATH, INGREDIENTS_DATA_FILEPATH, OFF_DUPLICATES_FILEPATH

with open(TAXONOMY_FILEPATH, 'r') as file:
    taxonomy = json.load(file)

with open(INGREDIENTS_DATA_FILEPATH, 'r') as file:
    ingredients_data = json.load(file)

MINIMUM_SCORE = 75  # Minimum fuzzy matching score for a match to be considered

result = pd.DataFrame(columns=['ingredient', 'reference'])

known_ingredients = pd.DataFrame([{'off_id': k,
                                   'fr': v['name'].get('fr'),
                                   'en': v['name'].get('en')}
                                  for k, v in taxonomy.items()
                                  if k in ingredients_data])

for ingredient_id, ingredient in tqdm.tqdm(taxonomy.items()):

    # Skipping already known ingredients
    if any(ingredient_id == known_ingredients.off_id):
        continue

    # Skipping additives
    if re.match(r'^en:e\d+', ingredient_id):
        continue

    # Custom rules
    if 'organic-' in ingredient_id:
        if any(ingredient_id.replace('organic-', '') == known_ingredients.off_id):
            result = result.append({'ingredient': ingredient_id,
                                    'reference': ingredient_id.replace('organic-', '')},
                                   ignore_index=True)

    if ('rice-' in ingredient_id) or ('-rice' in ingredient_id):
        result = result.append({'ingredient': ingredient_id,
                                'reference': 'en:rice'},
                               ignore_index=True)

    if ('egg-' in ingredient_id) or ('-egg' in ingredient_id):
        result = result.append({'ingredient': ingredient_id,
                                'reference': 'en:egg'},
                               ignore_index=True)

    if ('milk-' in ingredient_id) or ('-milk' in ingredient_id):
        result = result.append({'ingredient': ingredient_id,
                                'reference': 'en:milk'},
                               ignore_index=True)

    # Matching ingredients on id similarity
    matches = process.extract(ingredient_id, known_ingredients.off_id,
                              limit=50, scorer=fuzz.token_sort_ratio)

    # Filtering only best matches
    matches = [x for x in matches if x[1] >= MINIMUM_SCORE and x[0] != ingredient_id]

    for match in matches:
        result = result.append({'ingredient': ingredient_id,
                                'reference': match[0]},
                               ignore_index=True)

    # Matching on french name
    if 'fr' in ingredient['name']:
        matches = process.extract(ingredient['name']['fr'], known_ingredients.fr,
                                  limit=50, scorer=fuzz.token_sort_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE and x[0] != ingredient['name']['fr']]

        for match in matches:
            result = result.append({'ingredient': ingredient_id,
                                    'reference': known_ingredients[known_ingredients.fr == match[0]].iloc[
                                        0].off_id},
                                   ignore_index=True)

    # Matching on english name
    if 'en' in ingredient['name']:
        matches = process.extract(ingredient['name']['en'], known_ingredients.en,
                                  limit=50, scorer=fuzz.token_sort_ratio)

        # Filtering only best matches
        matches = [x for x in matches if x[1] >= MINIMUM_SCORE and x[0] != ingredient['name']['en']]

        for match in matches:
            result = result.append({'ingredient': ingredient_id,
                                    'reference': known_ingredients[known_ingredients.en == match[0]].iloc[
                                        0].off_id},
                                   ignore_index=True)

result.drop_duplicates(inplace=True)
result.sort_values(by='ingredient', inplace=True)

result.to_csv(OFF_DUPLICATES_FILEPATH, index=False)
