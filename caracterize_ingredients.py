""" Script to run the ingredients caracterization scripts in the right order. """

import ingredients_caracterization.nutri.nutri_from_ciqual.nutri_from_ciqual
import ingredients_caracterization.nutri.nutri_from_fcen.nutri_from_fcen
import ingredients_caracterization.nutri.nutri_from_manual_sources
import ingredients_caracterization.agribalyse.lci_from_agribalyse
import ingredients_caracterization.off_duplicates.off_duplicates_processing
import impacts_processing.ingredients_impacts
